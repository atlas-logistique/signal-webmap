// /**
//  * Ref:
//  * https://prettier.io/docs/en/options.html
//  */
module.exports = {
  endOfLine: "auto",
  arrowParens: "avoid",
  singleQuote: false, // default: false
  printWidth: 100,
  bracketSpacing: true,
  htmlWhitespaceSensitivity: "css",
  proseWrap: "preserve",
  semi: true,
  tabWidth: 2,
  trailingComma: "none",
  useTabs: false
};
