module.exports = {
  publicPath: `${process.env.BASE_URL}/`,
  pwa: {
    workboxOptions: {
      skipWaiting: true,
      runtimeCaching: [
        {
          urlPattern: /https:\/\/api\.mapbox\.com/,
          handler: "cacheFirst",
          options: {
            cacheName: "mapbox-cache",
            cacheableResponse: {
              statuses: [0, 200]
            },
            expiration: {
              maxEntries: 120
            }
          }
        },
        {
          urlPattern: /https:\/\/gitlab\.com\/api\/v4/,
          handler: "cacheFirst",
          options: {
            cacheName: "gitlab-cache",
            cacheableResponse: {
              statuses: [0, 200]
            },
            expiration: {
              maxEntries: 120
            }
          }
        }
      ]
    }
  }
};
