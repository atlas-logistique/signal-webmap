import { mount } from "@vue/test-utils";
import Map from "@/views/Map.vue";

describe("Map.vue", () => {
  window.URL.createObjectURL = function () {};
  // beforeEach(() => {
  //   cmp = Vue.extend(App); // Create a copy of the original component
  //   vm = new cmp({
  //     data: {
  //       // Replace data value with this fake data
  //       messages: ["Cat"]
  //     }
  //   }).$mount(); // Instances and mounts the component
  // });

  it("Map mounts well", () => {
    const wrapper = mount(Map, {});
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
