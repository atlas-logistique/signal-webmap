import maplibregl from "maplibre-gl";
import * as topojson from "topojson-client";
import store from "@/store";
import isoMapping from "@/../public/data/iso_map.json";
import { getTopojsonData } from "@/domain/topojsonReadFile";

export default {
  NO_DATA_COLOR: "#bbbbbb",

  availableAdmin0: function () {
    return store.getters["countries/getCountries"].map(c => c.pcode.toUpperCase());
  },
  countriesFilteredPCode: function (assessmentCategoryId) {
    return store.getters["countries/getCountries"]
      .filter(c => c.assessment_category.id === assessmentCategoryId)
      .map(c => c.pcode.toUpperCase());
  },

  getGeo: function (id) {
    return store.state.map.geos.find(geo => geo.id.toLowerCase() === id.toLowerCase());
  },

  getWorldGeo: function () {
    return store.state.map.geos.find(geo => geo.id === "world");
  },

  getSourceName: function (iso3, level) {
    if (iso3 === null) return "source-countries";
    return "source-" + iso3 + "-admin" + level;
  },

  getLayerName: function (iso3, level) {
    if (iso3 === null) return "countries";
    return "layer-" + iso3 + "-admin" + level;
  },

  getSourceUrl: function (iso3, level) {
    const currentAdminLevel = level.toString();
    if (iso3 === null) {
      return this.getWorldGeo().sourceUrl;
    }
    return this.getGeoBasedOnIso3(iso3).adminSources[currentAdminLevel];
  },

  getGeoMetaData: function (iso3 = null, level = store.getters["selection/getCurrentAdminLevel"]) {
    return {
      sourceUrl: this.getSourceUrl(iso3, level),
      source: this.getSourceName(iso3, level),
      layer: this.getLayerName(iso3, level)
    };
  },

  getGeoBasedOnIso3: function (iso3) {
    return store.state.map.geos.find(
      geo => typeof geo.iso !== "undefined" && geo.iso.toLowerCase() === iso3.toLowerCase()
    );
  },

  /**
   * Returns MaplibreGL expression filter to retrieve features based on active parent admin
   * @returns {array} maplibre expression filter (doc : https://maplibre.org/maplibre-gl-js-docs/style-spec/expressions/)
   */
  getAdmin0Filter: function () {
    return [
      "match",
      ["get", "iso_3166_1_alpha_3"],
      this.availableAdmin0().map(iso => iso.toUpperCase()),
      true,
      false
    ];
  },

  /**
   * Converts array of ISO3 to array of ISO2 (to query mapbox country-label A2 metadata)
   * @param {array} array Array which contains ISO3 of countries ['FRA', 'BEL', ...]
   */
  convertA3ArrayToA2: function (array) {
    let iso2Array = [];
    array.forEach(iso3 => {
      iso2Array.push(isoMapping[iso3.toUpperCase()]);
    });
    return iso2Array;
  },

  /**
   * Filters the active source features based on parent admin selected
   * @returns {Object} featuresCollection
   */
  getCurrentSourceFeatures: function (filter = true) {
    // Clone the the Geo object to manipulate independently the features
    const GEO = JSON.parse(JSON.stringify(store.getters["map/activeGeo"]));
    const featuresCollection =
      GEO.features[store.getters["selection/getCurrentAdminLevel"].toString()];
    let features = featuresCollection.features;
    if (filter) {
      // TODO: restore;
      const pcodeId = this.getIdBasedOnActiveAdminLevel(
        store.getters["selection/getCurrentAdminLevel"]
      );
      const activePCode = store.getters["selection/getCurrentAdminPCode"];
      features = features.filter(f => f.properties[pcodeId] === activePCode);
    }
    featuresCollection.features = features;
    return featuresCollection;
  },

  /**
   * Returns extent bounds of multiple bounds
   * @param {array} features Contains an array of features to calculate the extent bounds on
   */
  getMultipleFeaturesBounds(features) {
    let calcBounds = {
      _ne: {
        lng: -270,
        lat: -270
      },
      _sw: {
        lng: 270,
        lat: 270
      }
    };
    features.forEach(feature => {
      // Get country bounds
      let bounds = this.getBounds(feature.geometry);

      // If a new extent on North, east, south, west is found then update the bounds
      if (bounds._ne.lng > calcBounds._ne.lng) calcBounds._ne.lng = bounds._ne.lng;
      if (bounds._ne.lat > calcBounds._ne.lat) calcBounds._ne.lat = bounds._ne.lat;
      if (bounds._sw.lng < calcBounds._sw.lng) calcBounds._sw.lng = bounds._sw.lng;
      if (bounds._sw.lat < calcBounds._sw.lat) calcBounds._sw.lat = bounds._sw.lat;
    });
    return maplibregl.LngLatBounds.convert([
      [calcBounds._sw.lng, calcBounds._sw.lat],
      [calcBounds._ne.lng, calcBounds._ne.lat]
    ]);
  },

  /**
   * Returns the extent boundaries of a feature geometry
   * @param {Object} geometry from a e.features[0] element
   */
  getBounds(geometry) {
    let coordinates =
      geometry.type === "MultiPolygon" ? geometry.coordinates[0][0] : geometry.coordinates[0];
    return coordinates.reduce(function (bounds, coord) {
      return bounds.extend(coord);
    }, new maplibregl.LngLatBounds(coordinates[0], coordinates[0]));
  },

  /**
   * Returns feature property admin identifier based on admin level
   * @param {number} [0|1|2|3]? adminLevel
   * @returns {string} feature identifier property name
   */
  getIdBasedOnActiveAdminLevel: function (adminLevel = null) {
    if (adminLevel === null) {
      adminLevel = store.getters["selection/getCurrentAdminLevel"];
    }
    switch (adminLevel) {
      case 0:
        return "iso_3166_1_alpha_3";
      case 1:
        return "ADM1_PCODE";
      case 2:
        return "ADM2_PCODE";
      case 3:
        return "ADM3_PCODE";
    }
  },

  getIso3FromIso2: function (iso2) {
    iso2 = iso2.toUpperCase();
    let iso3Lower = null;
    Object.keys(isoMapping).forEach(iso3 => {
      if (isoMapping[iso3] === iso2) iso3Lower = iso3.toLowerCase();
    });
    return iso3Lower;
  },

  // ///////////////////////////////////////////////////////////////////////////////////////////
  // Compute country assessment colormap
  // ///////////////////////////////////////////////////////////////////////////////////////////
  computeCountryAssessementColorFilter: function () {
    // Compute Colormap
    const colorMap = [];
    for (const category of store.getters["assessmentCategories/getCategories"]) {
      const countryList = this.countriesFilteredPCode(category.id);
      if (countryList.length > 0) {
        colorMap.push(countryList);
        colorMap.push(category.color);
      }
    }
    // Add default color
    colorMap.push(this.NO_DATA_COLOR);

    // create the maplibre layer rule
    return ["match", ["get", "iso_3166_1_alpha_3"], ...colorMap];
  },
  // ///////////////////////////////////////////////////////////////////////////////////////////
  // Create Admin Layer
  // ///////////////////////////////////////////////////////////////////////////////////////////
  /**
   * Converts a Topojson to GeoJson file to preprocess it for MaplibreGL and D3
   * @param {string} iso3 Country iso3
   * @param {number} level Admin level of the source
   */
  async loadAdmin(iso3, level) {
    const currentIndicator = store.getters["selection/getCurrentIndicator"];
    const scheme = currentIndicator.description.group.scheme;
    const topoAdmin = await this.getAdminFeature(iso3.toLowerCase(), level);
    return this.processAdminFeature(topoAdmin, level, scheme, currentIndicator.dataId);
  },

  // ///////////////////////////////////////////////////////////////////////////////////////////
  // Add Admin Layer
  // ///////////////////////////////////////////////////////////////////////////////////////////

  addMapLayer: function (
    map,
    layerID,
    layerSourceID,
    layerSource,
    filter,
    level,
    hoverFillOpacity,
    layerBeforeId
  ) {
    map.addLayer(
      {
        id: layerID,
        type: "fill",
        source: layerSourceID,
        ...(layerSource && { "source-layer": layerSource }),
        layout: {},
        ...(level === 0 && { filter }),
        paint: {
          "fill-color": ["coalesce", ["get", "fill"], this.computeCountryAssessementColorFilter()],
          "fill-opacity": [
            "case",
            ["boolean", ["feature-state", "hover"], false],
            hoverFillOpacity,
            ["all", filter, ["!=", ["get", "fill"], null]],
            1,
            1
          ],
          "fill-outline-color": "#002E43"
        }
      },
      layerBeforeId
    );
  },
  async getAdminFeature(iso3, level) {
    const country = store.getters["countries/getCountryByPCode"](iso3);
    let topojson = country.topojsons[level];
    if (topojson === null) {
      topojson = await getTopojsonData(iso3, level);
      let topojsons = {};
      topojsons[level] = topojson;
      store.commit("countries/setTopojson", {
        iso3,
        topojsons
      });
    }

    return topojson;
  },
  processAdminFeature: function (topoAdmin, adminLevel, colorScheme, dataId) {
    const admin = topojson.feature(topoAdmin, topoAdmin.objects[Object.keys(topoAdmin.objects)[0]]);
    const dict = store.getters["selection/getCurrentCountryLviData"];
    admin.features.map(d => {
      const id = d.properties[this.getIdBasedOnActiveAdminLevel(adminLevel)];

      // Sets a property "id" in the features to have a consistent unique id attribute name
      d.properties.id = id;

      // If there is some LVI data for the boundaries
      if (Object.keys(dict).length) {
        const lvi = dict[id][dataId];

        if (typeof lvi !== "undefined") {
          // Add Heatmap colors based on LVI value
          let index = Math.floor(lvi * colorScheme.length);
          if (index === colorScheme.length) index = colorScheme.length - 1;
          d.properties.fill = colorScheme[index];
          if (isNaN(index)) {
            d.properties.fill = this.NO_DATA_COLOR;
          }
        }
      } else {
        d.properties.fill = this.NO_DATA_COLOR;
      }
    });
    return admin;
  }
};
