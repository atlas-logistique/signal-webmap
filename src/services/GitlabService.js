const PROJECT_ID = process.env.VUE_APP_GITLAB_PROJECT_ID;
const GITLAB_API = `https://gitlab.com/api/v4/projects/${PROJECT_ID}`;

const MAIN_BRANCH = "main";
const TOKEN = process.env.VUE_APP_GITLAB_TOKEN;

export const HEADERS = {
  headers: new Headers({
    "PRIVATE-TOKEN": window.atob(TOKEN),
    "Content-type": "application/json",
    Authorization: `Bearer ${window.atob(TOKEN)}`
  })
};

export async function getGitlabFile(fileUrl, type = FORMAT.JSON) {
  return new Promise((resolve, reject) => {
    fetch(`${GITLAB_API}/repository/files/${fileUrl}/raw?ref=${MAIN_BRANCH}`, {
      method: "GET",
      ...HEADERS
    })
      .then(response => {
        if (!response.ok) reject("error");
        return type === FORMAT.CSV ? response.text() : response.json();
      })
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      });
  });
}

// Gitlab folders, do not change
export const DATA_MAIN_FOLDER = "data";
export const TYPE_FOLDER = {
  GEO: "geo",
  LVI: "lvi"
};
export const FORMAT = {
  CSV: "csv",
  JSON: "json"
};

export function getGitlabFilePath(type = TYPE_FOLDER.LVI, iso3 = "bfa", adminLevel = 2) {
  const url = `${DATA_MAIN_FOLDER}/${type}/${iso3}/adm${adminLevel}.${getTypeFormat(type)}`;
  return encodeURIComponent(url);
}

export function getGeosConfigFilePath() {
  const url = `${DATA_MAIN_FOLDER}/geo_layers.json`;
  return encodeURIComponent(url);
}

export function getTypeFormat(type = TYPE_FOLDER.LVI) {
  return type === TYPE_FOLDER.LVI ? FORMAT.CSV : FORMAT.JSON;
}
