import * as d3 from "d3";

export const SCHEME_BLOCKS = 5;

export function getChromaticIndex(lviValue) {
  return Math.max(0, Math.min(SCHEME_BLOCKS - 1, Math.floor(lviValue / (1 / SCHEME_BLOCKS))));
}

export function getChromaticScale(color, number = SCHEME_BLOCKS) {
  let colorScheme = [];
  const f = d3
    .scaleSequential()
    .domain([0, number])
    .range([d3.rgb(color), d3.rgb("#EDEDED")]);

  for (let i = 0; i < number; i++) {
    colorScheme[i] = f(i);
  }
  return colorScheme;
}
