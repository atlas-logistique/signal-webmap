export const loadImage = (map, imageKey, imageURL) => {
  return new Promise((resolve, reject) => {
    map.loadImage(imageURL, (error, image) => {
      if (error) {
        reject(error);
      }
      map.addImage(imageKey, image);
      resolve();
    });
  });
};
