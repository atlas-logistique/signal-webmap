const DAY_IN_MILLISECONDS = 86400000;

export function dateMinus1day() {
  const timestamp = Date.now() - DAY_IN_MILLISECONDS;
  return new Date(timestamp).toISOString();
}

export function dateMinus7days() {
  const millisecondsToSubtract = 7 * DAY_IN_MILLISECONDS;
  const timestamp = Date.now() - millisecondsToSubtract;
  return new Date(timestamp).toISOString();
}

export function dateMinus3Months() {
  const resultDate = new Date();
  const month = resultDate.getMonth();
  const year = resultDate.getFullYear();
  const transitionalMonth = month - 3;
  const resultMonth = transitionalMonth < 0 ? transitionalMonth + 12 : transitionalMonth;
  const resultYear = transitionalMonth < 0 ? year - 1 : year;
  resultDate.setMonth(resultMonth);
  resultDate.setFullYear(resultYear);
  return resultDate.toISOString();
}
