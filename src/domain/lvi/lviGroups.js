import * as d3 from "d3";

// ////////////////////////////////////////////////////////////////////////////
// Create group
// ////////////////////////////////////////////////////////////////////////////

export function createGroup(id, color, scheme, name_fr, name_en) {
  return {
    id: id,
    color: color,
    scheme: scheme,
    name: {
      en: name_en,
      fr: name_fr
    }
  };
}
export function createMainGroup() {
  return createGroup(
    LVI_MAIN_GROUP_ID,
    d3.schemeOranges[5][4],
    d3.schemeOranges[5],
    "Groupe principal",
    "Main group"
  );
}

// ////////////////////////////////////////////////////////////////////////////
// Main Group Management
// ////////////////////////////////////////////////////////////////////////////

export const LVI_MAIN_GROUP_ID = "lvi";

export function replaceMainGroup(groups) {
  groups = removeMainGroup(groups);
  groups = addMainGroup(groups);
  return groups;
}

function removeMainGroup(groups) {
  return groups.filter(group => group.id.toUpperCase() !== LVI_MAIN_GROUP_ID.toUpperCase());
}

function addMainGroup(groups) {
  groups.push(createMainGroup());
  return groups;
}
