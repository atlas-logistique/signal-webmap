import store from "@/store";
import * as Papa from "papaparse";
import {
  getGitlabFile,
  getGitlabFilePath,
  getGeosConfigFilePath,
  TYPE_FOLDER,
  FORMAT
} from "@/services/GitlabService";
import { createLviData } from "./lviData";
import { ADMIN_LEVEL } from "./admins";

const sheetId = "1TtAWTTjjERhJ79WPhmwSOs-DBTw2jMIpeXzEyOAi4mY";
const exportFormat = "csv";
const lviFilename = `https://docs.google.com/spreadsheets/d/${sheetId}/gviz/tq?tqx=out:${exportFormat}&sheet=`;

const SHEET_NAMES = {
  mappingLabels: "mapping_labels",
  completenessLevel: "completeness_level"
};

/**
 * @param {*} filename Use filename if you parse data from an URL
 * @param {*} content  Use content only if you already have the content to parse
 */
const DEFAULT_PARSE_OPTIONS = {
  header: true,
  delimiter: ",",
  skipEmptyLines: true
};
async function parseFile(filename) {
  return new Promise((resolve, reject) => {
    Papa.parse(filename, {
      ...DEFAULT_PARSE_OPTIONS,
      download: true,
      complete(results) {
        if (results.data.length === 0) {
          resolve(null);
        } else {
          typeof results.data[0].alias === "undefined" ? resolve(results.data) : resolve(null);
        }
      },
      error(err) {
        resolve(null);
        reject(err);
      }
    });
  });
}
async function parseContent(content) {
  return new Promise((resolve, reject) => {
    Papa.parse(content, {
      ...DEFAULT_PARSE_OPTIONS,
      download: false,
      transform: function (value) {
        // Converts "3,4" into 3.4 (some tricks for removing trailing 0)
        const transformedValue = +parseFloat(value.replace(",", ".")).toFixed(2);
        if (isNaN(transformedValue)) {
          return value;
        }
        return transformedValue;
      },
      complete(results) {
        if (results.data.length === 0) {
          resolve(null);
        } else {
          typeof results.data[0].alias === "undefined" ? resolve(results.data) : resolve(null);
        }
      },
      error(err) {
        resolve(null);
        reject(err);
      }
    });
  });
}

export const GLOBAL_ADMIN_VALUE = "all";
export const GLOBAL_ADMIN_NAME = {
  en: "Global",
  fr: "Global"
};
export async function readAllLVIAdminDescription() {
  const lvi = {
    [GLOBAL_ADMIN_VALUE]: {
      name: GLOBAL_ADMIN_NAME
    }
  };

  // Read LVI data only for processed countries
  const countriesAssessed = store.getters["countries/getAssessedCountries"];
  for (const country of countriesAssessed) {
    const countryAdminAdescription = await readCountryLVIData(country);
    if (countryAdminAdescription[0]) {
      // Admin1 --> 0
      countryAdminAdescription[0].forEach(region => {
        lvi[region["adm1_pcode"]] = {
          name: {
            fr:
              "adm1_fr" in region
                ? region["adm1_fr"]
                : "adm1_en" in region
                ? region["adm1_en"]
                : region["adm1_pcode"],
            en:
              "adm1_en" in region
                ? region["adm1_en"]
                : "adm1_fr" in region
                ? region["adm1_fr"]
                : region["adm1_pcode"]
          }
        };
      });
    }
    // Admin2 --> 1
    if (countryAdminAdescription[1]) {
      countryAdminAdescription[1].forEach(region => {
        lvi[region["adm2_pcode"]] = {
          name: {
            fr:
              "adm2_fr" in region
                ? region["adm2_fr"]
                : "adm2_en" in region
                ? region["adm2_en"]
                : region["adm2_pcode"],
            en:
              "adm2_en" in region
                ? region["adm2_en"]
                : "adm2_fr" in region
                ? region["adm2_fr"]
                : region["adm2_pcode"]
          }
        };
      });
    }
  }
  return lvi;
}

async function getIndicatorData(iso3, adminLevel) {
  const filePath = getGitlabFilePath(TYPE_FOLDER.LVI, iso3, adminLevel);
  const csvData = await getGitlabFile(filePath, FORMAT.CSV);
  return await parseContent(csvData);
}

export async function readGeosConfigData() {
  const geosConfigFilePath = getGeosConfigFilePath();
  const jsonData = await getGitlabFile(geosConfigFilePath, FORMAT.JSON);
  console.log("jsonData", jsonData);
  return jsonData;
}

export async function readCountryLVIData(country) {
  const pcode = country.pcode.toLowerCase();

  return createLviData(
    await getIndicatorData(pcode, ADMIN_LEVEL.ADMIN_1),
    await getIndicatorData(pcode, ADMIN_LEVEL.ADMIN_2),
    await getIndicatorData(pcode, ADMIN_LEVEL.ADMIN_3)
  );
}

export async function readLVIMetadata() {
  return await parseFile(lviFilename + SHEET_NAMES.mappingLabels);
}

export async function readCompletenessLevel() {
  return await parseFile(lviFilename + SHEET_NAMES.completenessLevel);
}
