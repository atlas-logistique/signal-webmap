export const ADMIN_LEVEL = {
  ADMIN_1: 1,
  ADMIN_2: 2,
  ADMIN_3: 3
};

export const DEFAULT_ADMIN_LEVEL = ADMIN_LEVEL.ADMIN_2;
export const MAX_ADMIN_LEVEL = ADMIN_LEVEL.ADMIN_3;

export function pcodeIsCountry(pcode) {
  return pcode && pcode.length === 3;
}
