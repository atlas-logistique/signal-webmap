import * as Papa from "papaparse";
import { getChromaticScale } from "@/services/ThematicService";
import store from "@/store";
import {
  createMainGroup,
  createGroup,
  replaceMainGroup,
  LVI_MAIN_GROUP_ID
} from "@/domain/lvi/lviGroups";
import { isMainIndicator } from "@/domain/lvi/lviIndicators";
import { createIndicator } from "./lviIndicators";
import { createCountry } from "./lviCountries";

const sheetId = "18pAvN9Pg0AoGN-1_PRVpKEr61BRTUgG7yOVBgazecvs";
const exportFormat = "csv";
const lviFilename = `https://docs.google.com/spreadsheets/d/${sheetId}/gviz/tq?tqx=out:${exportFormat}&sheet=`;

const SHEET_NAMES = {
  countries: "countries",
  countryAssessmentCategory: "country_assessment_category",
  indicators: "indicator_info",
  indicatorGroup: "indicator_group",
  source: "indicator_source",
  thematic1: "catalog_thematic_1",
  thematic2: "catalog_thematic_2"
};

async function parseFile(filename) {
  return new Promise((resolve, reject) => {
    Papa.parse(filename, {
      header: true,
      delimiter: ",",
      download: true,
      skipEmptyLines: true,
      complete(results) {
        resolve(results.data);
      },
      error(err) {
        reject(err);
      }
    });
  });
}

export async function readCountriesData() {
  const result = await parseFile(lviFilename + SHEET_NAMES.countries);
  return result.map(country => {
    return createCountry(
      country.label,
      country.iso3,
      store.getters["assessmentCategories/getCategory"](country.assessment_category)
    );
  });
}

export async function readAssessmentCategoriesData() {
  const result = await parseFile(lviFilename + SHEET_NAMES.countryAssessmentCategory);
  return result.map(category => {
    return {
      color: category.color,
      id: category.id,
      is_assessed: category.is_assessed === "yes",
      label: {
        fr: category.label_fr,
        en: category.label_en
      }
    };
  });
}

export async function readIndicatorsData() {
  const result = await parseFile(lviFilename + SHEET_NAMES.indicators);
  return result.map(indicator => {
    return createIndicator(
      indicator.id,
      isMainIndicator(indicator),
      store.getters["lviGroups/getGroup"](indicator.group_id),
      indicator.countries.split(",").map(c => c.toLowerCase().trim()),
      {
        en: indicator.label_en,
        fr: indicator.label_fr
      },
      {
        en: indicator.definition_en,
        fr: indicator.definition_fr
      },
      {
        en: indicator.description_en,
        fr: indicator.description_fr
      },
      {
        en: indicator.tooltip_en,
        fr: indicator.tooltip_fr
      },
      {
        en: indicator.utilisation_en,
        fr: indicator.utilisation_fr
      }
    );
  });
}

export async function readIndicatorGroupData() {
  const result = await parseFile(lviFilename + SHEET_NAMES.indicatorGroup);

  const groups = result.map(group => {
    // TODO: Remove here lvi line and on gsheet groups
    if (group.id !== LVI_MAIN_GROUP_ID) {
      return createGroup(
        group.id,
        group.color,
        getChromaticScale(group.color),
        group.label_fr,
        group.label_en
      );
    } else {
      return createMainGroup();
    }
  });
  return replaceMainGroup(groups);
}

export async function readThematic1Data() {
  const result = await parseFile(lviFilename + SHEET_NAMES.thematic1);
  return result.map(thematic => {
    return {
      id: thematic.id,
      name: {
        en: thematic.label_en,
        fr: thematic.label_fr
      }
    };
  });
}

export async function readThematic2Data() {
  const result = await parseFile(lviFilename + SHEET_NAMES.thematic2);
  return result.map(thematic => {
    return {
      id: thematic.id,
      name: {
        en: thematic.label_en,
        fr: thematic.label_fr
      },
      parentId: thematic.thematic_lvl_1_id
    };
  });
}

export async function readSourceData() {
  const result = await parseFile(lviFilename + SHEET_NAMES.source);
  return result.map(source => {
    return {
      indicator: source.indicator_id,
      adminLevel: +source.admin_level,
      country: source.country,
      name: {
        en: source.label_en,
        fr: source.label_fr
      }
    };
  });
}
