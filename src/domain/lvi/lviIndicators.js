const LVI_MAIN_INDICATOR_ID = "LVI";

// ////////////////////////////////////////////////////////////////////////////
// Create Indicator
// ////////////////////////////////////////////////////////////////////////////

export function createCountryIndicator(indicator) {
  return {
    id: indicator.id,
    dataId: indicator.id,
    enabled: indicator.enabled,
    heatmap: null,
    description: indicator
  };
}

export function createIndicator(
  id,
  isMainIndicator,
  group,
  countries,
  label,
  definition,
  description,
  tooltip,
  utilisation
) {
  return {
    id: id,
    main: isMainIndicator,
    group: group,
    countries: countries,

    definition: definition,
    description: description,
    label: label,
    tooltip: tooltip,
    utilisation: utilisation
  };
}

// ////////////////////////////////////////////////////////////////////////////
// Indicator helpers
// ////////////////////////////////////////////////////////////////////////////

export function isMainIndicator(indicator) {
  return indicator && indicator.id === LVI_MAIN_INDICATOR_ID;
}
