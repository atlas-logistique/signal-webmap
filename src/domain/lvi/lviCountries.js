// ////////////////////////////////////////////////////////////////////////////
// Create Country
// ////////////////////////////////////////////////////////////////////////////

export function createCountry(label, iso3, assessmentCategory) {
  return {
    name: label,
    assessment_category: assessmentCategory,
    completness: [],
    dataSourceInfos: [],
    demography: null,
    indicators: [],
    indicatorMain: null,
    lviDataPerPCode: null,
    lviDataPerAdminLevel: null,
    lviMetadataPerAdminLevel: [],
    pcode: iso3,
    topojsons: {
      1: null,
      2: null,
      3: null
    }
  };
}
