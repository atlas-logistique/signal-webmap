// ////////////////////////////////////////////////////////////////////////////
// CreateLviData
// ////////////////////////////////////////////////////////////////////////////

export function createLviData(admin1Data, admin2Data, admin3Data) {
  return [admin1Data, admin2Data, admin3Data];
}

// ////////////////////////////////////////////////////////////////////////////
// LVI Data
// ////////////////////////////////////////////////////////////////////////////

export function getLviAdminsNames(lviData, adminLevel) {
  if (!lviData) {
    return [];
  }
  const levels = [];
  for (let i = 1; i <= adminLevel; i++) {
    levels.push(i);
  }

  return levels.map(level => lviData[`adm${level}_fr`]);
}

export function getLviAdminName(lviData, adminLevel) {
  if (!lviData) {
    return [];
  }

  return lviData[`adm${adminLevel}_fr`];
}
