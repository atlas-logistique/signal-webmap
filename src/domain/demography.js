export const DEMOGRAPHY_LVI_NAME = "pop_count";

export function computeDemographyRange(demographyPerAdminLevel) {
  for (const adminLevel in demographyPerAdminLevel) {
    const demography = Object.values(demographyPerAdminLevel[adminLevel]);

    // Compute min max
    const values = demography.map(item => item[DEMOGRAPHY_LVI_NAME]);
    const min = Math.min(...values);
    const max = Math.max(...values);

    // Compute range parameter
    const commonRatio = computeCommonRatio(min, max);
    const interval = computeInterval(min, commonRatio);

    // compute range values
    demography.map(item => {
      item.range = computeRangeValue(item[DEMOGRAPHY_LVI_NAME], interval) + 1;
    });
  }
}

const DEMOGRAPHY_INTERVAL_COUNT = 5;

function computeCommonRatio(min, max) {
  return Math.pow(max / min, 1 / (DEMOGRAPHY_INTERVAL_COUNT - 1));
}

function computeInterval(min, commonRatio) {
  const interval = [];
  for (let i = 0; i < DEMOGRAPHY_INTERVAL_COUNT; i++) {
    interval.push(Math.round(min * Math.pow(commonRatio, i)));
  }
  return interval;
}

function computeRangeValue(value, interval) {
  for (let i = 1; i < DEMOGRAPHY_INTERVAL_COUNT; i++) {
    if (value < interval[i]) {
      return i - 1;
    }
  }
  return DEMOGRAPHY_INTERVAL_COUNT - 1;
}
