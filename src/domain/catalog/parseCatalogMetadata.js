import { readThematic1Data, readThematic2Data } from "../lvi/lviConfigReadFile";
import store from "@/store";
import { GLOBAL_ADMIN_VALUE, readAllLVIAdminDescription } from "../lvi/lviReadFiles";

function newMapItem() {
  return {
    isExpanded: false,
    isSelected: false
  };
}

const EXCLUDED_ADMIN_VALUES = [""];

export async function parseCatalogMetadata(data) {
  const admins = await readAllLVIAdminDescription();

  const map = {};

  // ////////////////////////////////////////////////////////////
  // Parse the administrative level section
  // ////////////////////////////////////////////////////////////
  const countries = Object.assign(
    {},
    ...store.getters["countries/getCountries"].map(x => ({ [x.pcode]: x.value }))
  );

  const adminLevels = {
    documents: [],
    childs: [],
    parent: null,
    metadata: {
      id: "adminLevels",
      name: {
        en: "Administrative Level",
        fr: "Niveaux Administratifs"
      },
      path: `adminLevels`
    }
  };
  map[`adminLevels`] = newMapItem();

  data.forEach(document => {
    const documentKey = document.name.en;

    adminLevels.documents.push(documentKey);

    document.countries.forEach(country => {
      // Creates country entry if needed
      const path = `adminLevels/${country}`;
      const name = countries[country.toLowerCase()] ?? country;

      if (!(country in adminLevels.childs)) {
        adminLevels.childs[country] = {
          documents: [],
          childs: [],
          parent: adminLevels,
          metadata: {
            id: country,
            name: {
              en: name,
              fr: name
            },
            path: path
          }
        };
        map[path] = newMapItem();
      }

      // Add the document at the country level
      adminLevels.childs[country].documents.push(documentKey);

      // Add all admins
      if (document.admins.admin1) {
        const filteredAdmin1 = document.admins.admin1
          .split(";")
          .filter(admin1 => !EXCLUDED_ADMIN_VALUES.includes(admin1));
        //.filter(admin1 => admin1.includes(country)); // check if admin1 code (ex: 20BEN001) includes country iso3 code (ex: BEN)

        filteredAdmin1.forEach(admin1 => {
          // Add Admin1 if missing
          const path = `adminLevels/${country}/${admin1}`;
          if (!(admin1 in adminLevels.childs[country].childs)) {
            adminLevels.childs[country].childs[admin1] = {
              documents: [],
              childs: [],
              parent: adminLevels.childs[country],
              metadata: {
                id: admin1,
                name:
                  admin1 in admins
                    ? admins[admin1]["name"]
                    : {
                        en: admin1,
                        fr: admin1
                      },
                path: path
              }
            };
            map[path] = newMapItem();
          }

          // Add the document at the admin1 level
          adminLevels.childs[country].childs[admin1].documents.push(documentKey);

          // Add Admin2 if missing
          if (document.admins.admin2) {
            const filteredAdmin2 = document.admins.admin2
              .split(";")
              .filter(admin2 => !EXCLUDED_ADMIN_VALUES.includes(admin2))
              .filter(admin2 => admin2 !== GLOBAL_ADMIN_VALUE)
              .filter(admin2 => admin2.includes(admin1)); // check if admin2 code (ex: 20BFA056002) includes admin1 code (ex:20BFA056)

            filteredAdmin2.forEach(admin2 => {
              if (!(admin2 in adminLevels.childs[country].childs[admin1].childs)) {
                const path = `adminLevels/${country}/${admin1}/${admin2}`;
                adminLevels.childs[country].childs[admin1].childs[admin2] = {
                  documents: [],
                  childs: [],
                  parent: adminLevels.childs[country].childs[admin1],
                  metadata: {
                    id: admin2,
                    name:
                      admin2 in admins
                        ? admins[admin2]["name"]
                        : {
                            en: admin2,
                            fr: admin2
                          },
                    path: path
                  }
                };
                map[path] = newMapItem();
              }

              // Add the document at the admin2 level
              adminLevels.childs[country].childs[admin1].childs[admin2].documents.push(documentKey);
            });
          }
        });
      }
    });
  });

  // ////////////////////////////////////////////////////////////
  // Parse the thematics level section
  // ////////////////////////////////////////////////////////////

  const thematic1 = await readThematic1Data();
  const thematic2 = await readThematic2Data();

  const thematics = {
    documents: [],
    childs: [],
    parent: null,
    metadata: {
      id: "thematics",
      name: {
        en: "Thematics",
        fr: "Thématiques"
      },
      path: `thematics`
    }
  };
  map[`thematics`] = newMapItem();

  data.forEach(document => {
    const documentKey = document.name.en;
    thematics.documents.push(documentKey);
    document.thematic.forEach(thematic => {
      const foundin2 = thematic2.find(them => them.id === thematic);

      // LVL 2 thematic
      if (foundin2) {
        // create LVL 1 if it doesn't exist
        if (!(foundin2.parentId in thematics.childs)) {
          const found2in1 = thematic1.find(them => them.id === foundin2.parentId);
          if (found2in1) {
            const path = `thematics/${found2in1.id}`;
            thematics.childs[found2in1.id] = {
              documents: [],
              childs: [],
              parent: thematics,
              metadata: {
                ...found2in1,
                path: path
              }
            };
            map[path] = newMapItem();
          } else {
            console.log("matching error");
          }
        }

        // create LVL 2 if it doesn't exist
        if (!(foundin2.id in thematics.childs[foundin2.parentId].childs)) {
          const path = `thematics/${foundin2.parentId}/${foundin2.id}`;
          thematics.childs[foundin2.parentId].childs[foundin2.id] = {
            documents: [],
            childs: [],
            parent: thematics.childs[foundin2.parentId],
            metadata: {
              ...foundin2,
              path: path
            }
          };
          map[path] = newMapItem();
        }
        // add the LVL2 thematic
        thematics.childs[foundin2.parentId].documents.push(documentKey);
        thematics.childs[foundin2.parentId].childs[foundin2.id].documents.push(documentKey);
      } else {
        // LVL 1 thematic
        const foundin1 = thematic1.find(them => them.id === thematic);
        if (foundin1) {
          // create LVL 1 if it doesn't exist
          if (!(foundin1.id in thematics.childs)) {
            const path = `thematics/${foundin1.id}`;
            thematics.childs[foundin1.id] = {
              documents: [],
              childs: [],
              parent: thematics,
              metadata: {
                ...foundin1,
                path: path
              }
            };
            map[path] = newMapItem();
          }
          thematics.childs[foundin1.id].documents.push(documentKey);
        } else {
          console.log("not found");
        }
      }
    });
  });

  // ////////////////////////////////////////////////////////////
  // Parse the years section
  // ////////////////////////////////////////////////////////////
  const years = {
    documents: [],
    childs: [],
    parent: null,
    metadata: {
      id: "years",
      name: {
        en: "Years",
        fr: "Années"
      },
      path: `years`
    }
  };
  map[`years`] = newMapItem();

  data.forEach(document => {
    const documentKey = document.name.en;
    years.documents.push(documentKey);

    // Small hack to prevent casting from string to int and increase array length
    const year = `y${document.years.toString()}`;
    const yearRaw = `${document.years.toString()}`;

    // Creates country entry if needed
    if (!(year in years.childs)) {
      const path = `years/${year}`;
      years.childs[year] = {
        documents: [],
        childs: [],
        parent: years,
        metadata: {
          id: year,
          name: { en: yearRaw, fr: yearRaw },
          path: path
        }
      };
      map[path] = newMapItem();
    }
    years.childs[year].documents.push(documentKey);
  });

  return {
    filters: {
      years: years,
      adminLevels: adminLevels,
      thematics: thematics
    },
    map: map,
    documents: data
  };
}

export function filterCatalogSingle(catalog, filter) {
  const catalogFiltered = [];
  if (filter.admin2) {
    catalogFiltered.push(
      ...catalog.adminLevels.child[filter.country].childs[filter.admin1].childs[filter.admin2]
        .documents
    );
  } else if (filter.admin1) {
    catalogFiltered.push(
      ...catalog.adminLevels.child[filter.country].childs[filter.admin1].documents
    );
  } else if (filter.country) {
    catalogFiltered.push(...catalog.adminLevels.child[filter.country].documents);
  } else {
    catalogFiltered.push(...catalog.adminLevels.documents);
  }
  return catalogFiltered;
}

export function filterCatalogMultiple(catalog, filters) {
  /*
  filter : {
    countries,
    admin1,
    admin2
  }
  */
  const catalogFiltered = [];
  filters.forEach(filter => {
    if (filter.admin2) {
      catalogFiltered.push(
        ...catalog.adminLevels.child[filter.country].childs[filter.admin1].childs[filter.admin2]
          .documents
      );
    } else if (filter.admin1) {
      catalogFiltered.push(
        ...catalog.adminLevels.child[filter.country].childs[filter.admin1].documents
      );
    } else if (filter.country) {
      catalogFiltered.push(...catalog.adminLevels.child[filter.country].documents);
    } else {
      catalogFiltered.push(...catalog.adminLevels.documents);
    }
  });

  return [...new Set(catalogFiltered)];
}
