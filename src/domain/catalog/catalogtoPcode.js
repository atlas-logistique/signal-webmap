function extractPartAfterLastSlash(key) {
  const segments = key.split("/");
  return segments.pop();
}

export function catalogToPCode(catalogMap) {
  return Object.keys(catalogMap).map(catalogKey => extractPartAfterLastSlash(catalogKey));
}
