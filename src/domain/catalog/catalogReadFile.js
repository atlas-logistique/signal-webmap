const sheetNameCatalog = "resource_catalog";

const sheetId = "1mS-AhVWxB0bOjx-RJ0Q9tplqQxPuLFBeB2QDv6sEjEU";
const exportFormat = "csv";
const catalogFilename = `https://docs.google.com/spreadsheets/d/${sheetId}/gviz/tq?tqx=out:${exportFormat}&sheet=`;

import * as Papa from "papaparse";

async function parseFile(filename) {
  return new Promise((resolve, reject) => {
    Papa.parse(filename, {
      header: true,
      delimiter: ",",
      download: true,
      skipEmptyLines: true,
      complete(results) {
        resolve(results.data);
      },
      error(err) {
        reject(err);
      }
    });
  });
}

export async function readCatalogData() {
  const result = await parseFile(catalogFilename + sheetNameCatalog);
  return result.map(catalog => {
    return {
      name: {
        en: catalog.name_EN,
        fr: catalog.name_FR
      },
      action: catalog.action,
      format: catalog.format,
      link: catalog.link,
      admins: {
        admin1: catalog.admin1,
        admin2: catalog.admin2,
        admin3: catalog.admin3
      },
      thematic: catalog.thematic.split(";").map(e => e.trim()),
      countries: catalog.countries.split(";").map(e => e.trim()),
      years: catalog.years
    };
  });
}
