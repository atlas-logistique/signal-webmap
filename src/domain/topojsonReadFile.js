import { getGitlabFile, getGitlabFilePath, TYPE_FOLDER } from "@/services/GitlabService";

export async function getTopojsonData(iso3, adminLevel) {
  return await getGitlabFile(getGitlabFilePath(TYPE_FOLDER.GEO, iso3, adminLevel));
}
