export const LAYER_GDACS_ALL_STYLE_ID = "gdacs-all:style";
export const LAYER_GDACS_CYCLONES_STYLE_ID = "gdacs-cyclones:style";
export const LAYER_GDACS_FLOODS_STYLE_ID = "gdacs-floods:style";

export const LAYER_GDACS_ALL_SOURCE_ID = "gdacs-all:source";
export const LAYER_GDACS_CYCLONES_SOURCE_ID = "gdacs-cyclones:source";
export const LAYER_GDACS_FLOODS_SOURCE_ID = "gdacs-floods:source";

export const LAYER_GDACS_ICONS = {
  "cyclone-green": require("@/assets/icons/gdacs/cyclone_green.png"),
  "cyclone-orange": require("@/assets/icons/gdacs/cyclone_orange.png"),
  "cyclone-red": require("@/assets/icons/gdacs/cyclone_red.png"),
  "drought-green": require("@/assets/icons/gdacs/drought_green.png"),
  "drought-orange": require("@/assets/icons/gdacs/drought_orange.png"),
  "drought-red": require("@/assets/icons/gdacs/drought_red.png"),
  "earthquake-green": require("@/assets/icons/gdacs/earthquake_green.png"),
  "earthquake-orange": require("@/assets/icons/gdacs/earthquake_orange.png"),
  "earthquake-red": require("@/assets/icons/gdacs/earthquake_red.png"),
  "fire-green": require("@/assets/icons/gdacs/fire_green.png"),
  "fire-orange": require("@/assets/icons/gdacs/fire_orange.png"),
  "fire-red": require("@/assets/icons/gdacs/fire_red.png"),
  "flood-green": require("@/assets/icons/gdacs/flood_green.png"),
  "flood-orange": require("@/assets/icons/gdacs/flood_orange.png"),
  "flood-red": require("@/assets/icons/gdacs/flood_red.png"),
  "volcano-green": require("@/assets/icons/gdacs/volcano_green.png"),
  "volcano-orange": require("@/assets/icons/gdacs/volcano_orange.png"),
  "volcano-red": require("@/assets/icons/gdacs/volcano_red.png")
};

export const LAYER_GDACS_ALL_STYLE = {
  id: LAYER_GDACS_ALL_STYLE_ID,
  type: "symbol",
  source: LAYER_GDACS_ALL_SOURCE_ID,
  layout: {
    "icon-anchor": "bottom",
    "icon-allow-overlap": true,
    "icon-image": [
      "concat",
      [
        "match",
        ["get", "eventtype"],
        "EQ",
        "earthquake",
        "FL",
        "flood",
        "TC",
        "cyclone",
        "VO",
        "volcano",
        "DR",
        "drought",
        "fire"
      ],
      "-",
      ["match", ["get", "alertlevel"], "Green", "green", "Orange", "orange", "red"]
    ],
    visibility: "none"
  }
};

export const LAYER_GDACS_CYCLONES_STYLE = {
  id: LAYER_GDACS_CYCLONES_STYLE_ID,
  type: "symbol",
  source: LAYER_GDACS_CYCLONES_SOURCE_ID,
  layout: {
    "icon-anchor": "bottom",
    "icon-allow-overlap": true,
    "icon-image": [
      "match",
      ["get", "alertlevel"],
      "Green",
      "cyclone-green",
      "Orange",
      "cyclone-orange",
      "cyclone-red"
    ],
    visibility: "none"
  }
};

export const LAYER_GDACS_FLOODS_STYLE = {
  id: LAYER_GDACS_FLOODS_STYLE_ID,
  type: "symbol",
  source: LAYER_GDACS_FLOODS_SOURCE_ID,
  layout: {
    "icon-anchor": "bottom",
    "icon-allow-overlap": true,
    "icon-image": [
      "match",
      ["get", "alertlevel"],
      "Green",
      "flood-green",
      "Orange",
      "flood-orange",
      "flood-red"
    ],
    visibility: "none"
  }
};
