import centroid from "@turf/centroid";

export function computeDemographyPositions(demographyRangeMap, activeGeoFeatureCollection) {
  return {
    ...activeGeoFeatureCollection,
    features: activeGeoFeatureCollection.features
      .filter(feature => Object.keys(demographyRangeMap).includes(feature.properties.id))
      .map(feature => {
        return {
          ...centroid(feature),
          properties: {
            range: demographyRangeMap[feature.properties.id].range
          }
        };
      })
  };
}
