export const LAYER_DOCUMENTS_STYLE_ID = "documents:style";
export const LAYER_DOCUMENTS_SOURCE_ID = "documents:source";

export const LAYER_DOCUMENTS_STYLE = {
  id: LAYER_DOCUMENTS_STYLE_ID,
  type: "symbol",
  source: LAYER_DOCUMENTS_SOURCE_ID,
  minzoom: 5,
  layout: {
    "icon-image": "document",
    "icon-allow-overlap": true,
    "icon-anchor": "bottom",
    "icon-size": ["interpolate", ["linear"], ["zoom"], 5, 0.4, 10, 0.6, 15, 0.8, 20, 0.9],
    visibility: "none"
  }
};
