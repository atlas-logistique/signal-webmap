export const LAYER_DEMOGRAPHY_STYLE_ID = "demography:style";
export const LAYER_DEMOGRAPHY_SOURCE_ID = "demography:source";

export const LAYER_DEMOGRAPHY_STYLE = {
  id: LAYER_DEMOGRAPHY_STYLE_ID,
  type: "circle",
  source: LAYER_DEMOGRAPHY_SOURCE_ID,
  layout: {},
  paint: {
    "circle-radius": [
      "interpolate",
      ["linear"],
      ["zoom"],
      // when zoom is 0, set each feature's circle radius to the value of its "rating" property
      0,
      ["get", "range"],
      // when zoom is 10, set each feature's circle radius to four times the value of its "rating" property
      10,
      ["*", 4, ["get", "range"]]
    ],
    "circle-color": "#0077c8",
    "circle-opacity": 0.8,
    "circle-stroke-color": "#002e43",
    "circle-stroke-width": 1.5
  }
};
