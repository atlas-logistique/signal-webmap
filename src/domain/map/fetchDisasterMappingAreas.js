import { reproject } from "reproject";

export function fetchDisasterMappingAreas() {
  return fetch("https://smcs-unosat.web.cern.ch/api/aoi")
    .then(response => response.json())
    .then(disasterMappingAreas => reproject(disasterMappingAreas, "EPSG:3857", "EPSG:4326"));
}
