import { dateMinus1day } from "@/domain/date";

const GDACS_EVENTS_URL = "https://www.gdacs.org/gdacsapi/api/events/geteventlist/SEARCH";
const GDACS_ALERT_LEVELS = "Green;Orange;Red";

export function fetchGDACSEventData(from, to, eventType) {
  const params = `fromDate=${from}&toDate=${to}&alertlevel=${GDACS_ALERT_LEVELS}&eventlist=${eventType}`;
  let features = [];

  const requestGDACSEventData = () => {
    const page = Math.round(features.length / 100) + 1;
    const url = `${GDACS_EVENTS_URL}?${params}&pageNumber=${page}`;
    return fetch(url)
      .then(response => response.json())
      .then(data => {
        features = data.features ? features.concat(data.features) : features;
        return data.features && data.features.length === 100 ? requestGDACSEventData() : features;
      })
      .catch(error => console.error(error));
  };

  return requestGDACSEventData().then(() => ({ type: "FeatureCollection", features }));
}

export function filterLast24HoursGDACSEvent(gdacsEventFeatureCollection) {
  const toTimestamp = date => new Date(date).valueOf();
  const filter = limit => feature => toTimestamp(feature.properties.todate) > limit;
  const limitTimestamp = new Date(dateMinus1day()).valueOf();
  const filteredFeatures = gdacsEventFeatureCollection.features.filter(filter(limitTimestamp));
  return { type: "FeatureCollection", features: filteredFeatures };
}
