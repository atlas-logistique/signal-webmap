export const MAPLIBRE_ACCESS_TOKEN =
  "pk.eyJ1Ijoib3BzbWFwcGVyIiwiYSI6ImNrbW5xMWFuYzBqejMydnBnN2VjMTBjcG8ifQ.OtWWd9kzJdJjogrY7gb-sw";
export const MAPLIBRE_DEFAULT_PADDING = 40;

export const MAPLIBRE_DEFAULT_BASEMAP = {
  style: "mapbox://styles/opsmapper/ckz8bq8dn000u14p44yx9kdy8",
  overlayMinimumOpacity: 0.7,
  layerIdToInsertBefore: "waterway-shadow"
};

export const MAPLIBRE_SATELLITE_BASEMAP = {
  style: "mapbox://styles/opsmapper/clfqvupqz001601pl1hksrhho",
  overlayMinimumOpacity: 0.5,
  layerIdToInsertBefore: "waterway-shadow"
};

export const MAPLIBRE_BASEMAPS = {
  default: MAPLIBRE_DEFAULT_BASEMAP,
  satellite: MAPLIBRE_SATELLITE_BASEMAP
};

export const MAPLIBRE_CREDITS =
  '<strong>© <a href="https://cartong.org/" target="_blank"> CartONG </a></strong>';
