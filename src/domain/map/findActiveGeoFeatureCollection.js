export function findActiveGeoFeatureCollection(currentAdminLevel, activeGeoFeatures) {
  const emptyFeatureCollection = { type: "FeatureCollection", features: [] };
  const featuresForCurrentAdminLevel = activeGeoFeatures[currentAdminLevel] !== null;

  return featuresForCurrentAdminLevel
    ? activeGeoFeatures[currentAdminLevel]
    : emptyFeatureCollection;
}
