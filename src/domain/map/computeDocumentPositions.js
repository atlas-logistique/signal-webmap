import centroid from "@turf/centroid";
import { catalogToPCode } from "@/domain/catalog/catalogtoPcode";

export function computeDocumentPositions(catalogMap, activeGeoFeatureCollection) {
  const pcodes = catalogToPCode(catalogMap);
  return {
    ...activeGeoFeatureCollection,
    features: activeGeoFeatureCollection.features
      .filter(feature => pcodes.includes(feature.properties.id))
      .map(feature => centroid(feature))
  };
}
