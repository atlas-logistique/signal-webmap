export const LAYER_PRECIPITATION_STYLE_ID = "precipitation:style";
export const LAYER_PRECIPITATION_SOURCE_ID = "precipitation:source";

export const LAYER_PRECIPITATION_STYLE = {
  id: LAYER_PRECIPITATION_STYLE_ID,
  type: "raster",
  source: LAYER_PRECIPITATION_SOURCE_ID,
  paint: {
    "raster-opacity": 0.7
  }
};

export const LAYER_PRECIPITATION_SOURCE_TILES_URL =
  "https://www.gdacs.org/arcgis/rest/services/GDACS/daily_rain/MapServer/export?F=image&FORMAT=PNG32&TRANSPARENT=true&SIZE=256,256&BBOX={bbox-epsg-3857}&BBOXSR=3857&IMAGESR=3857&DPI=90";
