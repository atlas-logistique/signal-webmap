export const LAYER_DISASTER_MAPPING_FILL_STYLE_ID = "disaster-mapping:style:fill";
export const LAYER_DISASTER_MAPPING_LINE_STYLE_ID = "disaster-mapping:style:line";
export const LAYER_DISASTER_MAPPING_SOURCE_ID = "disaster-mapping:source";

export const LAYER_DISASTER_MAPPING_FILL_STYLE = {
  id: LAYER_DISASTER_MAPPING_FILL_STYLE_ID,
  type: "fill",
  source: LAYER_DISASTER_MAPPING_SOURCE_ID,
  paint: {
    "fill-color": [
      "match",
      ["get", "status"],
      "Completed",
      "rgb(176, 203, 79)",
      "In Progress",
      "rgb(252, 193, 25)",
      "Planned",
      "rgb(220, 88, 78)",
      "grey"
    ],
    "fill-opacity": 0.7
  }
};

export const LAYER_DISASTER_MAPPING_LINE_STYLE = {
  id: LAYER_DISASTER_MAPPING_LINE_STYLE_ID,
  type: "line",
  source: LAYER_DISASTER_MAPPING_SOURCE_ID,
  paint: {
    "line-color": [
      "match",
      ["get", "status"],
      "Completed",
      "rgb(176, 203, 79)",
      "In Progress",
      "rgb(252, 193, 25)",
      "Planned",
      "rgb(220, 88, 78)",
      "grey"
    ],
    "line-width": 2
  }
};
