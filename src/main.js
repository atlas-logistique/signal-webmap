import Vue from "vue";
import App from "@/App.vue";
import store from "@/store";
import {
  readCountriesData,
  readAssessmentCategoriesData,
  readIndicatorsData,
  readIndicatorGroupData,
  readSourceData
} from "@/domain/lvi/lviConfigReadFile";
import { readCatalogData } from "@/domain/catalog/catalogReadFile";
import { readCompletenessLevel, readGeosConfigData } from "@/domain/lvi/lviReadFiles";
import { loadLocaleMessages } from "@/i18n";
import VueI18n from "vue-i18n";
import { parseCatalogMetadata } from "@/domain/catalog/parseCatalogMetadata";

Vue.config.productionTip = false;

// App configuration
Vue.prototype.$config = {
  focalEmail: "atlas@hi.org"
};

const i18n = new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || "en",
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || "en",
  messages: loadLocaleMessages()
});

readGeosConfigData().then(geos => {
  store.commit("map/updateGeos", geos);

  readAssessmentCategoriesData().then(categories => {
    store.commit("assessmentCategories/setCategories", categories);

    readCountriesData().then(countries => {
      store.commit("countries/setCountries", countries);

      readCompletenessLevel().then(completeness => {
        store.commit("countries/populateWithCompletness", completeness);
      });

      readSourceData().then(dataSourceInfos => {
        store.commit("countries/populateWithdataSourceInfos", dataSourceInfos);
      });

      readCatalogData().then(data => {
        store.commit("catalog/setIsLoading", true);
        parseCatalogMetadata(data)
          .then(metadata => {
            store.commit("catalog/setMetadata", metadata);
          })
          .finally(() => {
            store.commit("catalog/setIsLoading", false);
          });

        readIndicatorGroupData().then(groups => {
          store.commit("lviGroups/setGroups", groups);
          readIndicatorsData().then(indicators => {
            store.commit("countries/populateWithIndicators", indicators);
            new Vue({
              store,
              i18n,
              render: h => h(App)
            }).$mount("#app");
          });
        });
      });
    });
  });
});
