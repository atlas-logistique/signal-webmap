import { LVI_MAIN_GROUP_ID } from "@/domain/lvi/lviGroups.js";

// ////////////////////////////////////////////////////////////////////////
// Groups
// ////////////////////////////////////////////////////////////////////////

// This is a local representation of the "indicator_group" tab
// https://docs.google.com/spreadsheets/d/18pAvN9Pg0AoGN-1_PRVpKEr61BRTUgG7yOVBgazecvs/edit#gid=832680159
// The content of a group item can be in domain/lvi/lviGroups

const module = {
  namespaced: true,
  state: () => ({
    lviGroups: []
  }),

  mutations: {
    setGroups(state, groups) {
      state.lviGroups = groups;
    }
  },
  actions: {},
  getters: {
    getMainGroup: state => state.lviGroups.find(group => LVI_MAIN_GROUP_ID === group.id),
    getGroup: state => groupId => state.lviGroups.find(group => groupId === group.id)
  }
};

export default module;
