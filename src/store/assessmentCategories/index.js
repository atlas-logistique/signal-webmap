/* eslint-disable no-unused-vars */
const moduleAssessmentCategories = {
  namespaced: true,
  state: {
    categories: []
  },

  mutations: {
    setCategories: function (state, categories) {
      state.categories = categories;
    }
  },
  actions: {},
  getters: {
    getCategories: state => state.categories,
    getCategory: state => categoryId => state.categories.find(element => element.id === categoryId)
  }
};

export default moduleAssessmentCategories;
