import store from "@/store";
import { findActiveGeoFeatureCollection } from "@/domain/map/findActiveGeoFeatureCollection";
import { ADDITIONAL_LAYERS_STYLES_ID } from "@/domain/map/additionalLayers";
import { computeDocumentPositions } from "@/domain/map/computeDocumentPositions";
import { computeDemographyPositions } from "@/domain/map/computeDemographyPositions";
import { fetchDisasterMappingAreas } from "@/domain/map/fetchDisasterMappingAreas";
import { fetchGDACSEventData, filterLast24HoursGDACSEvent } from "@/domain/map/gdacsEvents";
import { dateMinus3Months, dateMinus7days } from "@/domain/date";

export const LVI_ID = "LVI";

const moduleMap = {
  namespaced: true,
  state: {
    map: null,
    basemap: "default",

    featuresHaveChanged: false,
    geos: null,
    activeGeoId: "world",

    // specific layers
    disasterMappingFeatureCollection: { type: "FeatureCollection", features: [] },
    gdacsEventFeatureCollection: { type: "FeatureCollection", features: [] },
    gdacsCycloneFeatureCollection: { type: "FeatureCollection", features: [] },
    gdacsFloodFeatureCollection: { type: "FeatureCollection", features: [] },
    gdacsEventsFor24HoursOnly: false,
    gdacsCyclonesFor24HoursOnly: false,
    gdacsFloodsFor24HoursOnly: false
  },
  mutations: {
    // Change basemap
    changeBasemap(state, basemap) {
      state.basemap = basemap;
    },

    updateMap(state, map) {
      state.map = map;
    },
    updateGeos(state, geos) {
      state.geos = geos;
    },
    updateFeaturesHaveChanged(state) {
      state.featuresHaveChanged = !state.featuresHaveChanged;
    },
    updateActiveGeo(state, geoId) {
      state.activeGeoId = geoId;
    },
    updateActiveGeoFeatures(state, payload) {
      state.geos.forEach((geo, index) => {
        if (geo.id === state.activeGeoId) {
          state.geos[index].features[payload.level] = Object.assign({}, payload.features);
        }
      });
      state.featuresHaveChanged = !state.featuresHaveChanged;
    },

    // ////////////////////////////////////////////////////////////////////////
    // layers - generic
    // ////////////////////////////////////////////////////////////////////////

    setLayerVisibility(state, { layer, isVisible }) {
      if (!store.getters["map/isLayerLoaded"](layer)) {
        return;
      }
      state.map.setLayoutProperty(
        ADDITIONAL_LAYERS_STYLES_ID[layer],
        "visibility",
        isVisible ? "visible" : "none"
      );
    },

    // ////////////////////////////////////////////////////////////////////////
    // layers - specific
    // ////////////////////////////////////////////////////////////////////////

    loadDisasterMappingFeatureCollection(state) {
      if (state.disasterMappingFeatureCollection.features.length === 0) {
        this.commit("ui/activateLoading");
        fetchDisasterMappingAreas()
          .then(featureCollection => (state.disasterMappingFeatureCollection = featureCollection))
          .catch(error => console.error(error))
          .finally(() => this.commit("ui/deactivateLoading"));
      }
    },

    loadGDACSEventFor24HoursOnly(state, payload) {
      state.gdacsEventsFor24HoursOnly = payload;
    },

    loadGDACSCyclonesFor24HoursOnly(state, payload) {
      state.gdacsCyclonesFor24HoursOnly = payload;
    },

    loadGDACSFloodsFor24HoursOnly(state, payload) {
      state.gdacsFloodsFor24HoursOnly = payload;
    },

    loadGDACSEventFeatureCollection(state) {
      if (state.gdacsEventFeatureCollection.features.length === 0) {
        this.commit("ui/activateLoading");
        fetchGDACSEventData(dateMinus7days(), new Date().toISOString(), "")
          .then(featureCollection => (state.gdacsEventFeatureCollection = featureCollection))
          .catch(error => console.error(error))
          .finally(() => this.commit("ui/deactivateLoading"));
      }
    },

    loadGDACSCycloneFeatureCollection(state) {
      if (state.gdacsCycloneFeatureCollection.features.length === 0) {
        this.commit("ui/activateLoading");
        fetchGDACSEventData(dateMinus3Months(), new Date().toISOString(), "TC")
          .then(featureCollection => (state.gdacsCycloneFeatureCollection = featureCollection))
          .catch(error => console.error(error))
          .finally(() => this.commit("ui/deactivateLoading"));
      }
    },

    loadGDACSFloodFeatureCollection(state) {
      if (state.gdacsFloodFeatureCollection.features.length === 0) {
        this.commit("ui/activateLoading");
        fetchGDACSEventData(dateMinus3Months(), new Date().toISOString(), "FL")
          .then(featureCollection => (state.gdacsFloodFeatureCollection = featureCollection))
          .catch(error => console.error(error))
          .finally(() => this.commit("ui/deactivateLoading"));
      }
    }
  },
  actions: {},
  getters: {
    basemap(state) {
      return state.basemap;
    },

    // ////////////////////////////////////////////////////////////////////////
    // activeGeo
    // ////////////////////////////////////////////////////////////////////////

    activeGeo: state => state.geos.find(geo => geo.id === state.activeGeoId),

    activeGeoFeatureCollection(state, getters, rootState) {
      return findActiveGeoFeatureCollection(
        rootState.selection.currentAdminLevel,
        getters.activeGeo.features
      );
    },

    // ////////////////////////////////////////////////////////////////////////
    // layers - generic
    // ////////////////////////////////////////////////////////////////////////
    isLayerLoaded: state => layer =>
      state.map.getLayer(ADDITIONAL_LAYERS_STYLES_ID[layer]) !== undefined,

    isLayerVisible: (state, getters) => layer =>
      getters.isLayerLoaded(layer) &&
      state.map.getLayoutProperty(ADDITIONAL_LAYERS_STYLES_ID[layer], "visibility") === "visible",

    // ////////////////////////////////////////////////////////////////////////
    // layers - specific
    // ////////////////////////////////////////////////////////////////////////

    demographyFeatureCollection(state, getters, rootState) {
      return computeDemographyPositions(
        rootState.selection.currentDemographyRangeMap,
        getters.activeGeoFeatureCollection
      );
    },

    documentsFeatureCollection(state, getters, rootState) {
      return computeDocumentPositions(rootState.catalog.map, getters.activeGeoFeatureCollection);
    },

    disasterMappingFeatureCollection(state) {
      return state.disasterMappingFeatureCollection;
    },

    gdacsEvent24HoursFeatureCollection(state) {
      return filterLast24HoursGDACSEvent(state.gdacsEventFeatureCollection);
    },

    gdacsCyclone24HoursFeatureCollection(state) {
      return filterLast24HoursGDACSEvent(state.gdacsCycloneFeatureCollection);
    },

    gdacsFlood24HoursFeatureCollection(state) {
      return filterLast24HoursGDACSEvent(state.gdacsFloodFeatureCollection);
    },

    gdacsAllEventFeatureCollection(state, getters) {
      return state.gdacsEventsFor24HoursOnly
        ? getters.gdacsEvent24HoursFeatureCollection
        : state.gdacsEventFeatureCollection;
    },

    gdacsCycloneFeatureCollection(state, getters) {
      return state.gdacsCyclonesFor24HoursOnly
        ? getters.gdacsCyclone24HoursFeatureCollection
        : state.gdacsCycloneFeatureCollection;
    },

    gdacsFloodFeatureCollection(state, getters) {
      return state.gdacsFloodsFor24HoursOnly
        ? getters.gdacsFlood24HoursFeatureCollection
        : state.gdacsFloodFeatureCollection;
    }
  }
};
moduleMap;
export default moduleMap;
