import { GLOBAL_ADMIN_VALUE } from "@/domain/lvi/lviReadFiles";

function getNodeFromTree(tree, path) {
  const nodeKeys = path.split("/");
  let node = tree;
  nodeKeys.forEach(key => {
    if (key in node.childs) {
      node = node.childs[key];
    }
  });
  return node;
}

function updateChildrenSelectedValue(tree, isSelected, map) {
  map[tree.metadata.path].isSelected = isSelected;
  Object.keys(tree.childs).forEach(key => {
    updateChildrenSelectedValue(tree.childs[key], isSelected, map);
  });
}

function updateParentSelectedValue(tree, map) {
  if (tree && tree.parent) {
    const parentSelection = Object.keys(tree.parent.childs).reduce(
      (isSelected, key) => isSelected && map[tree.parent.childs[key].metadata.path].isSelected,
      true
    );
    map[tree.parent.metadata.path].isSelected = parentSelection;
    updateParentSelectedValue(tree.parent, map);
  }
}

function aggregateSelectedDocuments(tree, map) {
  // The current documents value contains all required docs
  if (map[tree.metadata.path].isSelected) {
    return { documents: tree.documents, hasSelection: true };
    // Per children
  } else {
    if (Object.keys(tree.childs).length > 0) {
      const documents = [];
      let hasSelection = false;

      Object.keys(tree.childs).forEach(key => {
        const aggregat = aggregateSelectedDocuments(tree.childs[key], map);
        documents.push(...aggregat.documents);
        hasSelection = hasSelection || aggregat.hasSelection;
      });
      return {
        documents: [...new Set(documents)],
        hasSelection: hasSelection
      };
    } else {
      // No Children -> return empty array
      return { documents: [], hasSelection: false };
    }
  }
}

const module = {
  namespaced: true,
  state: () => ({
    metadata: {},
    isLoading: false,
    tree: {
      documents: [],
      metadata: {
        id: "root",
        name: { en: "root" },
        path: ""
      },
      childs: [],
      parent: null
    },
    map: {},
    documents: {
      all: [],
      filtered: []
    }
  }),

  mutations: {
    setIsLoading: function (state, isLoading) {
      state.isLoading = isLoading;
    },

    setMetadata: function (state, metadata) {
      state.metadata = metadata;

      // update the tree structure
      Object.keys(metadata.filters).forEach(key => {
        const node = metadata.filters[key];
        state.tree.documents = [...new Set([...state.tree.documents, ...node.documents])];
        state.tree.childs[key] = node;
      });

      state.map = metadata.map;

      state.documents.all = metadata.documents;
      state.documents.filtered = metadata.documents;
    },

    setAdminLevelsFilter: function (state, { admin, country }) {
      // Clear current expand / selection
      Object.keys(state.map).forEach(key => {
        state.map[key].isExpanded = false;
        state.map[key].isSelected = false;
      });

      let path = Object.keys(state.map).find(key => key.endsWith(admin));

      // Compute the global path
      const globalPath = `adminLevels/${country.toUpperCase()}/${GLOBAL_ADMIN_VALUE}`;

      // if no document, path set to global
      if (!path) {
        path = globalPath;
        path = path in state.map ? path : undefined;
      }
      // if document, show documents and global
      if (path) {
        let pathToExpand = "";
        path.split("/").forEach(item => {
          pathToExpand = pathToExpand ? pathToExpand + "/" + item : item;
          this.commit("catalog/setExpanded", { path: pathToExpand, isExpanded: true });
        });
        this.commit("catalog/setExpanded", { path: pathToExpand, isExpanded: false });
        this.commit("catalog/setSelected", { path: pathToExpand, isSelected: true });
        this.commit("catalog/setSelected", { path: globalPath, isSelected: true });
      }
    },

    setExpanded: function (state, payload) {
      state.map[payload.path].isExpanded = payload.isExpanded;
    },

    setSelected: function (state, payload) {
      // Get associated node in strucure
      const node = getNodeFromTree(state.tree, payload.path);

      // Update all children with selection
      updateChildrenSelectedValue(node, payload.isSelected, state.map);

      // Update parent selected value
      updateParentSelectedValue(node, state.map);

      // Update parent selected value

      // - Retrieve Thematics selection
      const aggThematicsDocuments = aggregateSelectedDocuments(
        state.tree.childs["thematics"],
        state.map
      );
      const thematicsDocuments = aggThematicsDocuments.hasSelection
        ? aggThematicsDocuments.documents
        : state.tree.childs["thematics"].documents;

      // - Retrieve AdminLevels selection
      const aggAdminLevelsDocuments = aggregateSelectedDocuments(
        state.tree.childs["adminLevels"],
        state.map
      );
      const adminLevelsDocuments = aggAdminLevelsDocuments.hasSelection
        ? aggAdminLevelsDocuments.documents
        : state.tree.childs["adminLevels"].documents;

      // - Retrieve Years selection
      const yearsAggDocuments = aggregateSelectedDocuments(state.tree.childs["years"], state.map);
      const yearsDocuments = yearsAggDocuments.hasSelection
        ? yearsAggDocuments.documents
        : state.tree.childs["years"].documents;

      // Intersection of those selections
      const documentIndexes = thematicsDocuments
        .filter(doc => adminLevelsDocuments.includes(doc))
        .filter(doc => yearsDocuments.includes(doc));

      // For each key, populate the document indexes
      state.documents.filtered = state.documents.all.filter(document =>
        documentIndexes.includes(document.name.en)
      );
    }
  }
};

export default module;
