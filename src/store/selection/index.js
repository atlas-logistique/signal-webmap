import store from "@/store";
import { readCountryLVIData } from "@/domain/lvi/lviReadFiles";
import { DEFAULT_ADMIN_LEVEL } from "@/domain/lvi/admins";

const module = {
  namespaced: true,
  state: () => ({
    // Selection status
    // Helper for attribute watchers --> watch a single var, avoid duplciation.
    currentSelection: "empty",

    // Country
    currentCountry: null,

    // AdminLevel
    currentAdminLevel: DEFAULT_ADMIN_LEVEL,

    // AdminPCode --> clicked on map
    currentAdminPCode: null, // pcode

    // Indicators
    currentIndicator: null,

    // additional layers
    additionalLayers: [],

    // demography
    currentDemographyRangeMap: {}
  }),

  mutations: {
    // ////////////////////////////////////////////////////////////////////////
    // Country
    // ////////////////////////////////////////////////////////////////////////
    resetCurrentCountry(state) {
      state.currentCountry = null;
      state.currentIndicator = null;

      state.currentAdminLevel = DEFAULT_ADMIN_LEVEL;
      state.currentAdminPCode = null;

      state.currentDemographyRangeMap = {};

      state.currentSelection = "empty";
    },
    setCurrentCountry(state, country) {
      if (!country) {
        return;
      }
      this.commit("ui/activateLoading");

      // read lvi content, asynchroneous
      if (!country.lviDataPerPCode) {
        readCountryLVIData(country).then(data => {
          this.commit("countries/populateLVIDataForCountry", {
            country: country,
            lviData: data
          });
          state.currentCountry = country;
          state.currentIndicator = country.indicatorMain;
          state.currentAdminLevel = store.getters["selection/getDefaultAdminLevel"];
          state.currentAdminPCode = null;

          state.currentDemographyRangeMap =
            state.currentCountry.demography.perAdminLevel[state.currentAdminLevel] ?? {};

          state.currentSelection = `${state.currentCountry.pcode}-${state.currentAdminLevel}-${state.currentIndicator.id}`;
        });
        // already loaded, synchroneous
      } else {
        state.currentCountry = country;
        state.currentIndicator = country.indicatorMain;
        state.currentAdminLevel = store.getters["selection/getDefaultAdminLevel"];
        state.currentDemographyRangeMap =
          state.currentCountry.demography.perAdminLevel[state.currentAdminLevel] ?? {};
        state.currentAdminPCode = null;

        state.currentSelection = `${state.currentCountry.pcode}-${state.currentAdminLevel}-${state.currentIndicator.id}`;
      }
    },
    setCurrentCountryByPCode(state, pcode) {
      const country = store.getters["countries/getCountryByPCode"](pcode);
      if (country.assessment_category.is_assessed) {
        this.commit("selection/setCurrentCountry", country);
      }
    },

    // ////////////////////////////////////////////////////////////////////////
    // AdminLevel
    // ////////////////////////////////////////////////////////////////////////

    setCurrentAdminLevel(state, adminLevel) {
      state.currentAdminLevel = adminLevel;
      state.currentAdminPCode = null;
      state.currentDemographyRangeMap =
        state.currentCountry.demography.perAdminLevel[state.currentAdminLevel] ?? {};
      state.currentSelection = `${state.currentCountry.pcode}-${state.currentAdminLevel}-${state.currentIndicator.id}`;
    },

    // ////////////////////////////////////////////////////////////////////////
    // AdminPCode
    // ////////////////////////////////////////////////////////////////////////

    // AdminPCode
    setCurrentAdminPCode(state, pcode) {
      state.currentAdminPCode = pcode;
    },
    clearCurrentAdminPCode(state) {
      state.currentAdminPCode = null;
    },

    // ////////////////////////////////////////////////////////////////////////
    // Indicator
    // ////////////////////////////////////////////////////////////////////////

    setCurrentIndicator(state, indicator) {
      if (state.currentIndicator.id !== indicator.id) {
        state.currentIndicator = indicator;
      } else {
        state.currentIndicator = state.currentCountry.indicatorMain;
      }
      state.currentSelection = `${state.currentCountry.pcode}-${state.currentAdminLevel}-${state.currentIndicator.id}`;
    },

    // ////////////////////////////////////////////////////////////////////////
    // Additional layers
    // ////////////////////////////////////////////////////////////////////////

    enableAdditionalLayer(state, additionalLayer) {
      state.additionalLayers = [...state.additionalLayers, additionalLayer];
    },
    disableAdditionalLayer(state, additionalLayer) {
      state.additionalLayers = state.additionalLayers.filter(layer => layer !== additionalLayer);
    }
  },
  actions: {},
  getters: {
    // ////////////////////////////////////////////////////////////////////////
    // Country
    // ////////////////////////////////////////////////////////////////////////
    getCurrentCountry: state => state.currentCountry,
    getCurrentCountryPCode: state => (state.currentCountry ? state.currentCountry.pcode : null),
    hasCurrentCountry: state => state.currentCountry !== null,

    // ////////////////////////////////////////////////////////////////////////
    // Admin Level
    // ////////////////////////////////////////////////////////////////////////
    getCurrentAdminLevel: state => state.currentAdminLevel,

    // ////////////////////////////////////////////////////////////////////////
    // Admin PCode
    // ////////////////////////////////////////////////////////////////////////
    getCurrentAdminPCode: state => state.currentAdminPCode,
    hasCurrentAdminPCode: state => state.currentAdminPCode !== null,

    // ////////////////////////////////////////////////////////////////////////
    // Demography
    // ////////////////////////////////////////////////////////////////////////
    getCurrentDemographyRangeMap: state => state.currentDemographyRangeMap,
    getCurrentDemography: state => state.currentDemographyRangeMap[state.currentAdminPCode],
    getDemography: state => pcode => state.currentDemographyRangeMap[pcode],

    // ////////////////////////////////////////////////////////////////////////
    // Indicator Level
    // ////////////////////////////////////////////////////////////////////////
    getCurrentIndicator: state => state.currentIndicator,
    getCurrentIndicators: state => (state.currentCountry ? state.currentCountry.indicators : null),
    getCurrentIndicatorsByGroup: state => group =>
      state.currentCountry
        ? state.currentCountry.indicators.filter(
            indicator => indicator.description.group.id === group.id
          )
        : [],
    getCurrentIndicatorsByGroupId: state => groupId =>
      state.currentCountry
        ? state.currentCountry.indicators.filter(
            indicator => indicator.description.group.id === groupId
          )
        : [],

    // ////////////////////////////////////////////////////////////////////////
    // Completness
    // ////////////////////////////////////////////////////////////////////////
    getIndicatorCompleteness: (state, getter) => indicator => {
      if (!getter.hasCurrentCountry || !indicator) {
        return null;
      }
      return state.currentCountry.completeness.find(item => {
        return item.adm === `adm${state.currentAdminLevel}` && item.indicator_id === indicator.id;
      });
    },

    // ////////////////////////////////////////////////////////////////////////
    // dataSourceInfos
    // ////////////////////////////////////////////////////////////////////////
    getIndicatorDataSourceInfos: (state, getter) => indicator => {
      if (!getter.hasCurrentCountry || !indicator) {
        return null;
      }
      return state.currentCountry.dataSourceInfos.find(info => {
        return info.adminLevel == state.currentAdminLevel && info.indicator == indicator.id;
      });
    },

    // ////////////////////////////////////////////////////////////////////////
    // LviData
    // ////////////////////////////////////////////////////////////////////////
    // Country Lvi Data
    hasCurrentCountryLviData: state =>
      state.currentCountry && state.currentCountry.lviDataPerPCode !== null,
    getCurrentCountryLviData: state =>
      state.currentCountry ? state.currentCountry.lviDataPerPCode : null,

    // AdminLevel Lvi Data
    hasLviDataForAdminLevel: (state, getters) => adminLevel => {
      if (!getters.hasCurrentCountry) {
        return false;
      }
      return state.currentCountry.lviDataPerAdminLevel[adminLevel] !== undefined;
    },

    getDefaultAdminLevel: (state, getters) => {
      if (getters.hasLviDataForAdminLevel(DEFAULT_ADMIN_LEVEL)) {
        return DEFAULT_ADMIN_LEVEL;
      } else if (getters.hasLviDataForAdminLevel(DEFAULT_ADMIN_LEVEL - 1)) {
        return DEFAULT_ADMIN_LEVEL - 1;
      }
      console.error("No data was found for this country");
      return false;
    },
    getCurrentAdminLevelLviData: (state, getters) => {
      if (!getters.hasCurrentCountry) {
        return null;
      }
      return state.currentCountry.lviDataPerAdminLevel[state.currentAdminLevel];
    },

    // AdminPCode Lvi Data
    hasCurrentAdminPCodeLviData: (state, getters) => {
      return getters.getCurrentAdminPCodeLviData != null;
    },
    getCurrentAdminPCodeLviData: (state, getters) => {
      if (!getters.hasCurrentCountry || !getters.hasCurrentAdminPCode) {
        return null;
      }
      return state.currentCountry.lviDataPerPCode[state.currentAdminPCode];
    },

    // ////////////////////////////////////////////////////////////////////////
    // Additional layers
    // ////////////////////////////////////////////////////////////////////////
    additionalLayers(state) {
      return state.additionalLayers;
    }
  }
};

export default module;
