import { pcodeIsCountry } from "@/domain/lvi/admins";

export const LVI_ID = "LVI";

const moduleUi = {
  namespaced: true,
  state: {
    // ui state
    choroplethShown: true,
    detailedIndicatorsShown: false,
    infoPopupIndicator: null,
    isLoading: false,
    mapZoomHelperShown: false,
    hoveredPCode: null,
    popup: null,
    mapMove: false
  },
  mutations: {
    // Loading
    activateLoading(state) {
      state.isLoading = true;
    },
    deactivateLoading(state) {
      state.isLoading = false;
    },
    // Hovered PCode
    resetHoveredPCode(state) {
      state.hoveredPCode = null;
    },
    setHoveredPCode(state, hoveredPCode) {
      state.hoveredPCode = hoveredPCode;
    },
    // Map Move
    setMapMove(state, mapMove) {
      state.mapMove = mapMove;
    },

    setPopup(state, popup) {
      state.popup = popup;
    },
    updateChoroplethShown(state, choroplethShown) {
      state.choroplethShown = choroplethShown;
    },
    updateInfoPopupIndicator(state, indicator) {
      state.infoPopupIndicator = indicator;
    },
    updateDetailedIndicatorsShown(state, detailedIndicatorsShown) {
      state.detailedIndicatorsShown = detailedIndicatorsShown;
    },
    updateMapZoomHelperShown(state) {
      state.mapZoomHelperShown = true;

      window.clearTimeout(timeoutHandle);
      timeoutHandle = window.setTimeout(() => {
        state.mapZoomHelperShown = false;
      }, 1500);
    }
  },
  actions: {},
  getters: {
    // ////////////////////////////////////////////////////////////////////////
    // Hovered PCode
    // ////////////////////////////////////////////////////////////////////////
    hasHoveredPCode(state) {
      return state.hoveredPCode != null;
    },
    getHoveredPCode(state) {
      return state.hoveredPCode;
    },

    getHoveredPCodeData(state, getters, rootState, rootGetters) {
      if (pcodeIsCountry(state.hoveredPCode)) {
        return rootGetters["countries/getCountryByPCode"](state.hoveredPCode);
      } else {
        return rootGetters["selection/getCurrentCountryLviData"][state.hoveredPCode];
      }
    },

    // ////////////////////////////////////////////////////////////////////////
    // ui state
    // ////////////////////////////////////////////////////////////////////////

    hasChoroplethShown(state) {
      return state.choroplethShown;
    },

    isLoading(state) {
      return state.isLoading;
    },

    isMapMoving(state) {
      return state.mapMove;
    },

    // ////////////////////////////////////////////////////////////////////////
    // info popup
    // ////////////////////////////////////////////////////////////////////////

    hasInfoPopup(state) {
      return state.infoPopupIndicator !== null;
    },
    getInfoPopupIndicator(state) {
      return state.infoPopupIndicator;
    }
  }
};

var timeoutHandle = window.setTimeout(() => {
  moduleUi.state.mapZoomHelperShown = false;
}, 1500);

moduleUi;
export default moduleUi;
