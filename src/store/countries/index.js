import { createCountryIndicator } from "@/domain/lvi/lviIndicators";
import { DEMOGRAPHY_LVI_NAME, computeDemographyRange } from "@/domain/demography";

const module = {
  namespaced: true,
  state: () => ({
    countries: []
  }),

  mutations: {
    populateWithCompletness(state, completness) {
      state.countries.forEach(country => {
        country.completeness = completness.filter(
          item => item.country.toLowerCase() === country.pcode
        );
      });
    },
    populateWithdataSourceInfos(state, dataSourceInfos) {
      state.countries.forEach(country => {
        country.dataSourceInfos = dataSourceInfos.filter(
          item => item.country.toLowerCase() === country.pcode
        );
      });
    },
    populateWithIndicators(state, indicators) {
      state.countries.forEach(country => {
        // main indicator
        country.indicatorMain = createCountryIndicator(
          indicators.find(indicator => indicator.main)
        );
        // lvi indicators
        country.indicators = indicators
          .filter(indicator => indicator.group.id !== "other")
          .filter(indicator => indicator.countries.includes(country.pcode) && !indicator.main)
          .map(indicator => createCountryIndicator(indicator));

        // demography indicator
        country.demography = indicators.find(indicator => indicator.id === DEMOGRAPHY_LVI_NAME);

        // other indicators
        country.otherIndicatorsById = indicators
          .filter(indicator => indicator.group.id === "other")
          .map(indicator => createCountryIndicator(indicator))
          .reduce((acc, indicator) => ({ ...acc, [indicator.id]: indicator }), {});
      });
    },
    populateLVIDataForCountry(state, payload) {
      const country = payload.country;
      const lviData = payload.lviData;

      // Fill in lviData structures
      country.lviDataPerAdminLevel = {};
      country.lviDataPerPCode = {};

      lviData.forEach((adminLevel, level) => {
        if (adminLevel !== null) {
          country.lviDataPerAdminLevel[level + 1] = adminLevel;
          adminLevel.forEach(admin => {
            country.lviDataPerPCode[admin["adm" + (level + 1) + "_pcode"]] = admin;
          });
        }
      });

      // Compute lvi demography
      country.demography.perAdminLevel = {};

      lviData.forEach((adminLevel, level) => {
        if (adminLevel !== null) {
          country.demography.perAdminLevel[level + 1] = {};
          adminLevel.forEach(item => {
            const pcode = item["adm" + (level + 1) + "_pcode"];
            country.demography.perAdminLevel[level + 1][pcode] = {
              [DEMOGRAPHY_LVI_NAME]: item[DEMOGRAPHY_LVI_NAME],
              pcode: pcode,
              range: 0 // computed later
            };
          });
        }
      });

      computeDemographyRange(country.demography.perAdminLevel);
    },
    setCountries: function (state, countries) {
      state.countries = countries;
    },
    setTopojson: function (state, { iso3, topojsons }) {
      state.countries.forEach((country, key) => {
        if (country.pcode === iso3) {
          state.countries[key] = {
            ...{ ...country },
            ...{ topojsons: { ...country.topojsons, ...topojsons } }
          };
        }
      });
    }
  },
  actions: {},
  getters: {
    getCountries: state => state.countries,
    getAssessedCountries: state =>
      state.countries.filter(country => country.assessment_category.is_assessed),
    getCountryByPCode: state => pcode => {
      return state.countries.find(country => country.pcode === pcode.toLowerCase());
    }
  }
};

export default module;
