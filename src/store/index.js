import Vue from "vue";
import Vuex from "vuex";

import moduleAssessmentCategories from "./assessmentCategories";
import moduleCountries from "./countries";
import moduleLviGroups from "./lviGroups";
import moduleCatalog from "./catalog";
import moduleMap from "./map";
import moduleSelection from "./selection";
import moduleUi from "./ui";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    assessmentCategories: moduleAssessmentCategories,
    catalog: moduleCatalog,
    countries: moduleCountries,
    lviGroups: moduleLviGroups,
    map: moduleMap,
    selection: moduleSelection,
    ui: moduleUi
  }
});
